package cz.berescak.adventofcode.utils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileUtil {
    private final static String basePath = "src/main/resources/";

    public static Stream<String> getResourceLineStream(String path) {
        try {
            return  Files.lines(Paths.get(basePath  + path));
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getResourceAllInLineStream(String path) {
            return getResourceLineStream(path).collect(Collectors.joining());
    }
}
