package cz.berescak.adventofcode.year2017.day10;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Day10B {
    private static Integer[] input = {34,88,2,222,254,93,150,0,199,255,39,32,137,136,1,167};
    private static int listSize = 256;
    private static int position = 0;
    private static int skipSize = 0;

    public static void main(String[] args) {
        System.out.println(resolve());
    }



    private static List<Integer> processHash(List<Integer> list, List<Integer> input) {
        for (int length : input) {
            List<Integer> sectionPositions = new ArrayList<>();
            List<Integer> listToReverse = new ArrayList<>();
            for (int i = 0; i < length; i++) {
                int pos = position % list.size();
                sectionPositions.add(pos);
                listToReverse.add(list.get(pos));
                position++;
            }

            Collections.reverse(listToReverse);
            int item = 0;
            for (Integer p : sectionPositions) {
                list.set(p, listToReverse.get(item));
                item++;
            }

            position += skipSize;
            skipSize++;
        }
        return list;
    }

    static String resolve() {
        position = 0;
        skipSize = 0;
        String in = getInputAsString();

        List<Integer> lengths = new ArrayList<>();
        for (char c : in.toCharArray()) {
            lengths.add((int) c);
        }
        Integer[] append = {17, 31, 73, 47, 23};
        lengths.addAll(Arrays.asList(append));

        List<Integer> list = generateArray(listSize);
        List<Integer> sparseHash = new ArrayList<>();
        for (int i = 0; i < 64; i++) {
            sparseHash = processHash(list, lengths);
        }

        List<Integer> denseHash = getDenseHash(sparseHash);

        StringBuilder knotHash = new StringBuilder();
        for (int val : denseHash) {
            String hex = getHexValueFor(val);
            knotHash.append(hex);
        }
        return knotHash.toString();
    }

    private static List<Integer> getDenseHash(List<Integer> sparseHash) {
        int pos = 0;
        int out;
        List<Integer> denseHash = new ArrayList<>();
        while (pos < listSize) {
            out = sparseHash.get(pos);
            for (int i = pos + 1; i < pos + 16; i++) {
                out = out ^ sparseHash.get(i);
            }
            denseHash.add(out);
            pos += 16;
        }
        return denseHash;
    }

    private static String getInputAsString() {
        StringBuilder inputStr = new StringBuilder();
        for (Integer in : input) {
            inputStr.append(in).append(",");
        }
        inputStr.deleteCharAt(inputStr.length() - 1);
        return inputStr.toString();
    }

    private static String getHexValueFor(int val) {
        return Integer.toHexString(val).length() < 2 ? "0" + Integer.toHexString(val) : Integer.toHexString(val);
    }

    private static List<Integer> generateArray(int length) {
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < length; i++) {
            list.add(i);
        }
        return list;
    }
}
