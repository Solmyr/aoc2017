package cz.berescak.adventofcode.year2017.day10;

import java.util.Arrays;

public class Day10A {
    private static int[] testLengths = {3, 4, 1, 5};
    private static int[] lengths = {34,88,2,222,254,93,150,0,199,255,39,32,137,136,1,167};

    public static void main(String[] args) {
        int[] array1 = generateArray(4);
        resolve(array1, testLengths);

        int[] array2 = generateArray(255);
        resolve(array2, lengths);

    }

    private static void resolve(int[] array, int[] lengths) {
        int pointer = 0;
        int arrayLen = array.length;
        for (int i = 0; i < lengths.length; i++) {
            int len = lengths[i];

            int[] tempArray = new int[len];

            for (int j = 0; j < len; j++) {
                tempArray[j] = array[(pointer + len - j - 1) % arrayLen];
            }


            for (int j = 0; j < len; j++) {
                array[(pointer + j) % arrayLen] = tempArray[j];
            }


            pointer += len + i;

        }

        System.out.println("RESULT: " + (array[0] * array[1]));
    }

    private static int[] generateArray(int max) {
        int[] array = new int[max + 1];
        for (int i = 0; i <= max ; i++) {
            array[i] = i;
        }

        return array;
    }


}
