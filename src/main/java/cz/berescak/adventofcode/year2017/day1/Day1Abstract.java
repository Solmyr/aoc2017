package cz.berescak.adventofcode.year2017.day1;

public abstract class Day1Abstract {

    protected String resolve(final String input) {
        int sum = 0;
        int len = input.length();
        int step = getStep(len);

        for (int i = 0; i < len; i++) {
            if(input.charAt(i) == input.charAt((i+step)%len)) sum += Character.getNumericValue(input.charAt(i));
        }

        return "SUM: " + sum;
    }

    protected abstract int getStep(int length);
}
