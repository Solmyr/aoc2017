package cz.berescak.adventofcode.year2017.day6;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Day6B {

    public static void main(String[] args) {
        int[] input =  {5,1,10,0,1,7,13,14,3,12,8,10,7,12,0,6};

        System.out.println("Test: " + resolve(input));
    }

    private static String resolve(int[] blocks) {
        int len = blocks.length;
        List<String> combinations = new ArrayList<>();

        while(true) {
            int index = getMaxIndex(blocks);
            int value = blocks[index];

            blocks[index] = 0;

            for (int i = index + 1; i < index + 1 + value; i++) {
                blocks[i % len]++;
            }

            String scombination = toString(blocks);
            if(combinations.contains(scombination))
                return "" +  ((combinations.size()) - combinations.indexOf(scombination));
            combinations.add(scombination);
        }

    }

    private static String toString(int[] blocks) {
        String s = "";
        for (int i = 0; i < blocks.length; i++) {
            s += blocks[i] + "x";
        }
        return s;
    }

    private static int getMaxIndex(int[] blocks) {
        int max = blocks[0];
        int maxIndex = 0;

        for (int i = 1; i < blocks.length; i++) {
            if(max < blocks[i]) {
                max = blocks[i];
                maxIndex = i;
            }
        }

        return maxIndex;

    }

}
