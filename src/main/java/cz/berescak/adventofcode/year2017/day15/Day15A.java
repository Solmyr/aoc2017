package cz.berescak.adventofcode.year2017.day15;

public class Day15A {
    public static void main(String[] args) {
        test();
        resolve();
    }

    private static void resolve() {
        Generator genA = new Generator(16807, 512);
        Generator genB = new Generator(48271, 191);


        int count = 0;
        for (int i = 0; i < 40_000_000; i++) {
            long a = genA.getNext();
            long b = genB.getNext();
            if(a % 65536 == b % 65536) count++;
            if(i % 100_000 == 0) System.out.println("" + i + "...");
        }

        System.out.println("RESULT: " + count);
    }

    private static void test() {
        Generator genA = new Generator(16807, 65);
        Generator genB = new Generator(48271, 8921);


        int count = 0;
        for (int i = 0; i < 40_000_000; i++) {
            long a = genA.getNext();
            long b = genB.getNext();
            if(a % 65536 == b % 65536) count++;
            if(i % 100_000 == 0) System.out.println("" + i + "...");
        }

        System.out.println("TEST: " + count);
    }
}
