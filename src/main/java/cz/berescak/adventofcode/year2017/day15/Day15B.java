package cz.berescak.adventofcode.year2017.day15;

public class Day15B {
    public static void main(String[] args) {
        test();
        resolve();
    }

    private static void resolve() {
        Generator genA = new Generator(16807, 512);
        Generator genB = new Generator(48271, 191);


        int count = 0;
        for (int i = 0; i < 5_000_000; i++) {
            long a = genA.getNextMultiple(4);
            long b = genB.getNextMultiple(8);

            if(a % 65536 == b % 65536) count++;
            if(i % 100_000 == 0) System.out.println("" + i + "...");
        }

        System.out.println("RESULT: " + count);
    }

    private static void test() {
        Generator genA = new Generator(16807, 65);
        Generator genB = new Generator(48271, 8921);


        int count = 0;
        for (int i = 0; i < 5_000_000; i++) {
            long a = genA.getNextMultiple(4);
            long b = genB.getNextMultiple(8);
            if(a % 65536 == b % 65536) count++;
            if(i % 100_000 == 0) System.out.println("" + i + "...");
        }

        System.out.println("TEST: " + count);
    }
}
