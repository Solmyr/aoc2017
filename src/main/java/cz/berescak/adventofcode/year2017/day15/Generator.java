package cz.berescak.adventofcode.year2017.day15;

public class Generator {
    long value;
    long factor;

    public Generator(long factor, long startValue) {
        this.factor = factor;
        this.value = startValue;
    }

    public long getNext() {
        value = (value * factor) % 2147483647L;
        return value;
    }

    public long getValue() {
        return value;
    }

    public long getNextMultiple(int multiple) {
        while(true) {
            if(getNext() % multiple == 0) return getValue();
        }
    }
}
