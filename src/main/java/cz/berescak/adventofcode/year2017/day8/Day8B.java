package cz.berescak.adventofcode.year2017.day8;

import cz.berescak.adventofcode.utils.FileUtil;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

public class Day8B {
    public static void main(String[] args) {
        System.out.println("TEST: " + resolve(FileUtil.getResourceLineStream("day8Atest")));

        System.out.println("TEST: " + resolve(FileUtil.getResourceLineStream("day8")));
    }

    private static String resolve(Stream<String> stream) {
        Map<String, Integer> registers = new HashMap<>();



        stream.forEach(line -> processLine(line, registers));

        Integer max = registers.values()
                .stream()
                .mapToInt(i -> i)
                .max()
                .orElse(0);

        return "maxActual = " + max + " maxGlobal = " + registers.get("___MAX___");
    }

    private static void processLine(String line, Map<String, Integer> registers) {
        String[] splited = line.split("\\s+");

        String register = splited[0];
        String operator = splited[1];
        int operand = Integer.parseInt(splited[2]);

        String ifregister = splited[4];
        String ifoperator = splited[5];
        int ifoperand = Integer.parseInt(splited[6]);




        int ifregValue = getRegisterValue(ifregister, registers);

        boolean ifresult = false;
        switch (ifoperator) {
            case ">": ifresult = ifregValue > ifoperand; break;
            case "<": ifresult = ifregValue < ifoperand; break;
            case "==": ifresult = ifregValue == ifoperand; break;
            case ">=": ifresult = ifregValue >= ifoperand; break;
            case "<=": ifresult = ifregValue <= ifoperand; break;
            case "!=": ifresult = ifregValue != ifoperand; break;
        }

        if (ifresult) {
            int regValue = getRegisterValue(register, registers);
            if(operator.equals("inc")) {
                regValue += operand;
            } else
            {
                regValue -= operand;
            }
            registers.put(register, regValue);

            int max = getRegisterValue("___MAX___", registers);
            if(max < regValue) registers.put("___MAX___", regValue);
        }



    }

    private static int getRegisterValue(String register, Map<String, Integer> registers) {
        if(!registers.containsKey(register)) {
            registers.put(register, 0);
        }

        return registers.get(register);
    }
}
