package cz.berescak.adventofcode.year2017.day7;

import com.google.common.collect.*;
import cz.berescak.adventofcode.utils.FileUtil;
import lombok.Data;
import lombok.ToString;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Day7B {
    public static void main(String[] args) {
        System.out.println("Test: " + resolve("day7"));
    }

    private static String resolve(String path) {
        Stream<String> lines = FileUtil.getResourceLineStream(path);

        Map<String, Tower> allTowers = new HashMap<>();

        lines.forEach(line -> parseTower(line, allTowers));

        allTowers.values()
                .stream()
                .forEach(tow -> setMastering(tow, allTowers));
        ;


        Tower top = allTowers.values()
                .stream()
                .filter(tow -> tow.getMastertower() == null)
                .findFirst()
                .orElse(null);

        countTotalSize(top, allTowers);


        Tower problematicTopNode = findProblem(top, allTowers).stream()
                .sorted(Comparator.comparingInt(Tower::getTotalSize))
                .findFirst()
                .orElse(null);


        Multimap<Integer, Tower> problematicChilds = MultimapBuilder.treeKeys().arrayListValues().build();

        problematicTopNode.getSubtowers()
                .stream()
                .map(name -> allTowers.get(name))
                .forEach(tow -> problematicChilds.put(tow.getTotalSize(), tow));

        System.out.println(problematicChilds);
        // uz je nalezenej problematickej (cumah) a 3 ktery jsou OK -> uz staci rucne odecist totalsize problematickyho
        // a OK a toto odecist od vlastni velikosti problematickyho

/*
        System.out.println("CUMAH" + allTowers.get("cumah"));

        System.out.println("pqmyjtl" + allTowers.get("pqmyjtl"));
        System.out.println("wczux" + allTowers.get("wczux"));
        System.out.println("rfjilj" + allTowers.get("rfjilj"));
*/


        return "";
    }

    private static List<Tower> findProblem(Tower top, Map<String, Tower> allTowers) {
        List<Tower> problematic = new ArrayList<>();

        if(top.getSubtowers().size() > 2) {
            Tower first = allTowers.get(top.getSubtowers().get(0));

            for (int i = 1; i < top.getSubtowers().size(); i++) {
                if(first.getTotalSize() != allTowers.get(top.getSubtowers().get(i)).getTotalSize()) {
                    problematic.add(top);
                    break;
                }
            }
        }

        top.getSubtowers().stream().forEach(tow -> problematic.addAll(findProblem(allTowers.get(tow), allTowers)));
        return problematic;
    }

    private static int countTotalSize(Tower top, Map<String, Tower> allTowers) {
        int sumSubTowers = top.getSubtowers()
                                .stream()
                                .mapToInt(sub -> countTotalSize(allTowers.get(sub), allTowers))
                                .sum();

        top.setTotalSize(top.getSize() + sumSubTowers);
        return top.getTotalSize();
    }

    private static void setMastering(Tower tow, Map<String, Tower> allTowers) {
        tow.getSubtowers()
            .stream()
            .forEach(subtowerString -> allTowers.get(subtowerString).setMastertower(tow));
    }

    private static void parseTower(String line, Map<String, Tower> allTowers) {
        Tower t = new Tower();
        String[] array = line.split("\\s+");

        t.setSize(Integer.parseInt(array[1].replace("(", "").replace(")", "")));

        t.setName(array[0]);
        for (int i = 3; i < array.length; i++) {
            t.getSubtowers().add(array[i].replace(",", ""));

        }

        allTowers.put(t.getName(), t);


    }


    @Data
    @ToString(exclude = "mastertower")
    private static class Tower {
        private String name;
        private List<String> subtowers = new ArrayList<>();
        private Tower mastertower;
        private int size;
        private int totalSize;

    }


}
