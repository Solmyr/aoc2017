package cz.berescak.adventofcode.year2017.day7;

import cz.berescak.adventofcode.utils.FileUtil;
import lombok.Data;

import java.io.File;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;

public class Day7A {
    public static void main(String[] args) {
        System.out.println("Test: " + resolve("day7ATest"));
        System.out.println("Test: " + resolve("day7"));
    }

    private static String resolve(String path) {
        Stream<String> lines = FileUtil.getResourceLineStream(path);

        Map<String, Tower> allTowers = new HashMap<>();

        lines.forEach(line -> parseTower(line, allTowers));

        allTowers.values()
                .stream()
                .forEach(tow -> setMastering(tow, allTowers));
        
        

        
        return "" + allTowers.values()
                .stream()
                .filter(tow -> tow.getMastertower() == null)
                .findFirst()
                .orElse(null)
                .getName();

    }

    private static void setMastering(Tower tow, Map<String, Tower> allTowers) {
        tow.getSubtowers()
            .stream()
            .forEach(subtowerString -> allTowers.get(subtowerString).setMastertower(tow));
    }

    private static void parseTower(String line, Map<String, Tower> allTowers) {
        Tower t = new Tower();
        String[] array = line.split("\\s+");

        t.setName(array[0]);
        for (int i = 3; i < array.length; i++) {
            t.getSubtowers().add(array[i].replace(",", ""));
        }

        allTowers.put(t.getName(), t);


    }


    @Data
    private static class Tower {
        private String name;
        private Set<String> subtowers = new HashSet<>();
        private Tower mastertower;
    }


}
