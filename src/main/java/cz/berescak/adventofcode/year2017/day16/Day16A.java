package cz.berescak.adventofcode.year2017.day16;

import cz.berescak.adventofcode.utils.FileUtil;


public class Day16A {
    public static void main(String[] args) {
        String[] instructions = FileUtil.getResourceAllInLineStream("day16").split(",");


        DancePermutator input = new DancePermutator(16);
        for (int i = 0; i < 1_000_000 ; i++) {
            for (int j = 0; j < instructions.length; j++) {
                input.doInstruction(instructions[j]);
            }

            if(i == 0) input.print();
            if(i % 1000 == 0) System.out.println("Hototvo: " + i);

        }
        input.print();
    }
}
