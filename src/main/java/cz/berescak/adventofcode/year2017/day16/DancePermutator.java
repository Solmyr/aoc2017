package cz.berescak.adventofcode.year2017.day16;

import java.util.Arrays;

public class DancePermutator {
    private char[] array;

    public DancePermutator(int len) {
        init(len);
    }

    private void init(int len) {
        array = new char[len];

        for (int i = 0; i < len; i++) array[i] = (char) ((int) 'a' + i);
    }

    public void spin(int spinLen) {
        char[] newArray = new char[array.length];
        for (int i = 0; i < array.length; i++) newArray[(i + spinLen) % array.length] = array[i];
        array = newArray;
    }

    public void exchange(int a, int b) {
        char tmp = array[a];
        array[a] = array[b];
        array[b] = tmp;
    }

    public void partner(char a, char b) {
        exchange(findIndex(a), findIndex(b));
    }

    private int findIndex(char c) {
        for (int i = 0; i < array.length; i++) if (array[i] == c) return i;
        return -1;
    }

    public void print() {
        System.out.println(Arrays.toString(array));
    }

    public void doInstruction(String instruction) {
        switch (instruction.charAt(0)) {
            case 's':
                int spinLen = Integer.parseInt(instruction.substring(1));
                spin(spinLen);
                break;
            case 'x': {
                String[] temp = instruction.substring(1).split("/");
                exchange(Integer.parseInt(temp[0]), Integer.parseInt(temp[1]));
                break;
            }
            case 'p': {
                String[] temp = instruction.substring(1).split("/");
                partner(temp[0].charAt(0),temp[1].charAt(0));
                break;
            }
        }

    }

    public char[] getArray() {
        return Arrays.copyOf(array,array.length);
    }


}
