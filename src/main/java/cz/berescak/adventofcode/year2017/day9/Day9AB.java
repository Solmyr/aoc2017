package cz.berescak.adventofcode.year2017.day9;

import cz.berescak.adventofcode.utils.FileUtil;

public class Day9AB {
    public static void main(String[] args) {
        FileUtil.getResourceLineStream("day9all").forEach(line -> resolve(line));
    }

    private static void resolve(String line) {
        boolean openGarbage = false;
        int score = 0;
        int openGroups = 0;
        int garbageCharCount = 0;


        for (int i = 0; i < line.length(); i++) {
            char c = line.charAt(i);

            if(c == '!') {
                i++;
                continue;
            }

            if(openGarbage) {
                if(c == '>') {
                    openGarbage = false;
                } else garbageCharCount++;
                continue;
            }


            else if(c == '<') openGarbage = true;
            else if(c == '{') openGroups++;
            else if(c == '}') {
                score += openGroups;
                openGroups--;
            }
         }

        System.out.println(" SCORE: " + score + " GARB CHARS: " + garbageCharCount);
    }
}
