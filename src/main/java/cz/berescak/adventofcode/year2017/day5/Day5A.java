package cz.berescak.adventofcode.year2017.day5;

import cz.berescak.adventofcode.utils.FileUtil;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Day5A {
    public static void main(String[] args) {

        System.out.println("TEST: " + resolve(FileUtil.getResourceLineStream("day5Atest")));
        System.out.println("TEST: " + resolve(FileUtil.getResourceLineStream("day5")));
    }

    private static int resolve(Stream<String> resourceLineStream) {
        List<Integer> instructions = resourceLineStream
                .map(line -> Integer.parseInt(line))
                .collect(Collectors.toList());

            int index = 0;
            int counter = 0;
            while(index < instructions.size()) {
                int value = instructions.get(index);

                instructions.set(index, value + 1);
                index += value;
                counter++;
            }

            return counter;
        }
}
