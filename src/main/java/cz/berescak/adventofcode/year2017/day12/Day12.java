package cz.berescak.adventofcode.year2017.day12;

import cz.berescak.adventofcode.utils.FileUtil;
import lombok.Getter;
import lombok.Setter;

import java.util.*;
import java.util.stream.Stream;

public class Day12 {
    public static void main(String[] args) {
        resolve(FileUtil.getResourceLineStream("day12"));
    }

    private static void resolve(Stream<String> stream) {
        Map<Integer, Node> nodes = new HashMap<>();

        stream.forEach(line -> addNode(nodes, line));

        List<Set<Integer>> groups = new ArrayList<>();

        for (Node node : nodes.values()) {
            if(isInGroup(node, groups)) continue;

            Set<Integer> group = new HashSet<>();
            createGroup(node, group);
            groups.add(group);
        }

        for (Set<Integer> group : groups) {
            if(group.contains(0)) {
                System.out.println("Result A = " + group.size());
                break;
            }
        }
        System.out.println("Result B = " + groups.size());

    }

    private static void createGroup(Node node, Set<Integer> group) {
        group.add(node.name);

        for (Node childNode : node.connected) {
            if(!group.contains(childNode.name)) createGroup(childNode, group);
        }
    }

    private static boolean isInGroup(Node node, List<Set<Integer>> groups) {
        for (Set<Integer> group : groups) {
            if(group.contains(node.name)) return true;
        }
        return false;
    }

    private static void addNode(Map<Integer, Node> nodes, String line) {
        String[] array = line.split("\\s+");

        int nodeName = Integer.parseInt(array[0]);
        Node node;
        if(nodes.containsKey(nodeName)) node = nodes.get(nodeName);
        else {
            node = new Node();
            node.setName(nodeName);
        }
        nodes.put(nodeName, node);

        for (int i = 2; i < array.length; i++) {
            int node2Name = Integer.parseInt(array[i].replace(",", ""));

            Node node2;
            if(nodes.containsKey(node2Name)) node2 = nodes.get(node2Name);
            else {
                node2 = new Node();
                node2.setName(node2Name);
            }
            nodes.put(node2Name, node2);

            node.getConnected().add(node2);
            node2.getConnected().add(node);
        }




    }


    @Getter
    @Setter
    private static class Node {
        private Integer name;
        private Set<Node> connected = new HashSet<>();

        public String toString() {
            String s = "NODE " + name + " SUBNODES[";
            for (Node node : connected) {
                s += node.getName() + ", ";
            }
            s+="]\n";
            return s;
        }

    }

}
