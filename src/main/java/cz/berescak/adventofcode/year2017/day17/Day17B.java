package cz.berescak.adventofcode.year2017.day17;

import org.magicwerk.brownies.collections.GapList;

import java.util.ArrayList;
import java.util.List;

public class Day17B {
    public static void main(String[] args) {
        resolve(3);
        resolve(354);
    }

    private static void resolve(int step) {
        List<Integer> list = new GapList<>();
        list.add(0);
        int position = 0;

        for (int i = 1; i <= 50_000_000; i++) {
            position = (position + step) % list.size();
            position++;
            list.add(position,i);

            if(i % 1_000_000 == 0)System.out.println("" + i);
        }

        System.out.println("RESENI: " + list.get(1));

    }
}
