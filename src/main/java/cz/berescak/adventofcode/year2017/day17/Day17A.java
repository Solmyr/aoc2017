package cz.berescak.adventofcode.year2017.day17;

import java.util.ArrayList;
import java.util.List;

public class Day17A {
    public static void main(String[] args) {
        resolve(3);
        resolve(354);
    }

    private static void resolve(int step) {
        List<Integer> list = new ArrayList<>();
        list.add(0);
        int position = 0;

        for (int i = 1; i <= 2017; i++) {
            position = (position + step) % list.size();
            position++;
            list.add(position,i);

            System.out.println(list);
        }

        System.out.println("RESENI: " + list.get(position + 1));

    }
}
