package cz.berescak.adventofcode.year2017.day3;

public class Day3B {
    private static int[][] matrix;
    private static final int HALF_SIZE = 10;


    public static void main(String[] args) {
        matrix = new int[HALF_SIZE * 2][HALF_SIZE * 2];

        set(0, 0, 1);

        for (int i = 2;  ; i++) {
            int[] coord = indexToCoord(i);
            int x = coord[0];
            int y = coord[1];

            int sum = 0;
            sum += get(x + 1, y);
            sum += get(x + 1, y + 1);
            sum += get(x + 1, y - 1);
            sum += get(x, y + 1);
            sum += get(x, y - 1);
            sum += get(x - 1, y + 1);
            sum += get(x - 1, y - 1);
            sum += get(x - 1, y);

            set(x,y, sum);

            if(sum > 347991) {
                System.out.println("x: " + x + " y: " + y + " value: " + sum);
                break;
            }

        }
    }

    private static int get(int x, int y) {
        return matrix[x + HALF_SIZE][y + HALF_SIZE];
    }

    private static void set(int x, int y, int value) {
        matrix[x + HALF_SIZE][y + HALF_SIZE] = value;
    }


    private static int[] indexToCoord(int in) {
        int bottomLevel = (int) ((Math.sqrt(in - 1) - 1) / 2);
        int bottomEdge = 2 * bottomLevel + 1;
        int diff = in - (bottomEdge * bottomEdge) - 1;

        int x;
        int y;

        int newEdge = bottomEdge + 2;

        if (diff >= 2 * bottomEdge + newEdge + 1) {
            x = diff - 2 * bottomEdge - 2 * newEdge + 1;
            y = -1;
        } else if (diff >= bottomEdge + newEdge) {
            x = -(bottomEdge + 1);
            y = -diff + 3 * bottomEdge + 1;
        } else if (diff >= bottomEdge + 1) {
            x = -(diff - bottomEdge);
            y = bottomEdge;
        } else {
            x = 0;
            y = diff;
        }

        x += bottomLevel + 1;
        y -= bottomLevel;

        int[] point = new int[2];
        point[0] = x;
        point[1] = y;
        return point;
    }

}

