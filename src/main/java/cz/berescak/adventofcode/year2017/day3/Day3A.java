package cz.berescak.adventofcode.year2017.day3;

public class Day3A {

    public static void main(String[] args) {
        System.out.println(resolve(12));
        System.out.println(resolve(23));
        System.out.println(resolve(1024));
        System.out.println(resolve(347991));


    }

    private static String resolve(int in) {
        int bottomLevel = (int) ((Math.sqrt(in - 1) - 1 )/ 2);
        int bottomEdge = 2*bottomLevel + 1;
        int diff = in - (bottomEdge * bottomEdge) - 1 ;

        int x = -1;
        int y = -1;

        int newEdge = bottomEdge + 2;

        if(diff >= 2 * bottomEdge + newEdge + 1) {
            x =  diff - 2* bottomEdge - 2*newEdge  + 1 ;
            y = -1;
        } else
        if(diff >= bottomEdge + newEdge ) {
            x = - (bottomEdge  + 1);
            y = - diff + 3*bottomEdge +1;
        } else if(diff >= bottomEdge + 1) {
            x = - (diff - bottomEdge);
            y = bottomEdge;
        } else {
            x = 0;
            y = diff;
        }

        x += bottomLevel + 1;
        y -= bottomLevel;

        int result = Math.abs(x) + Math.abs(y);

        return "IN: " +  in + "X: " + x + " Y: " + y + " res: " + result;
    }

}

