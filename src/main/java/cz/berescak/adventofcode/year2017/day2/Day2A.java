package cz.berescak.adventofcode.year2017.day2;


import cz.berescak.adventofcode.utils.FileUtil;

import java.util.Arrays;
import java.util.stream.Stream;

public class Day2A {

    public static void main(String[] args) {
        System.out.println("TEST 1: " + resolve(FileUtil.getResourceLineStream("day2Atest")));
        System.out.println("MY INPUT" + resolve(FileUtil.getResourceLineStream("day2")));
    }

    private static String resolve(Stream<String> stream) {
       int sum = stream.mapToInt(line -> findDiff(line)).sum();
       return "SUM: " + sum;
    }

    private static int findDiff(String line) {
        int[] arrays = Arrays.stream(line.split("\\s+"))
                            .mapToInt(a -> Integer.parseInt(a))
                            .toArray();

        int min = Arrays.stream(arrays)
                .min()
                .orElse(0);

        int max = Arrays.stream(arrays)
                .max()
                .orElse(0);

        return max - min;
    }
}
