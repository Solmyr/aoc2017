package cz.berescak.adventofcode.year2017.day2;

import cz.berescak.adventofcode.utils.FileUtil;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class Day2B {

    public static void main(String[] args) {
        System.out.println("TEST 1: " + resolve(FileUtil.getResourceLineStream("day2Btest")));
        System.out.println("MY INPUT" + resolve(FileUtil.getResourceLineStream("day2")));
    }

    private static String resolve(Stream<String> stream) {
        int sum = stream.mapToInt(line -> findDiff(line)).sum();
        return "SUM: " + sum;
    }

    private static int findDiff(String line) {
        List<Integer> sortedInts = Arrays.stream(line.split("\\s+"))
                .map(a -> Integer.parseInt(a))
                .sorted()
                .collect(toList());

        while (sortedInts.size() > 1) {
            int i = checkList(sortedInts);
            if (i > 0)
                return i;

            sortedInts.remove(0);
        }
        return 0;
    }

    private static int checkList(List<Integer> sortedInts) {
        Integer first = sortedInts.get(0);

        for (int i = 1; i < sortedInts.size(); i++) {
            Integer second = sortedInts.get(i);
            if (second / first > 1 && second % first == 0) return second / first;
        }
        return 0;
    }
}
