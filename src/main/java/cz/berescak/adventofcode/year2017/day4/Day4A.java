package cz.berescak.adventofcode.year2017.day4;

import cz.berescak.adventofcode.utils.FileUtil;

import java.io.File;
import java.util.Set;
import java.util.TreeSet;

public class Day4A {
    public static void main(String[] args) {
        System.out.println("TEST (aa bb cc dd ee): " + resolve("aa bb cc dd ee"));
        System.out.println("TEST (aa bb cc dd aa): " + resolve("aa bb cc dd aa"));
        System.out.println("TEST (aa bb cc dd aaa): " + resolve("aa bb cc dd aaa"));

        int result =  FileUtil.getResourceLineStream("day4")
                .mapToInt(line -> resolve(line))
                .sum();

        System.out.println("RESULT: " + result);

    }

    private static int resolve(String input) {
        Set<String> words = new TreeSet<>();

        for (String word : input.split("\\s+")) {
            if(words.contains(word)) {
                return 0;
            }

            words.add(word);
        }

        return 1;
    }
}
