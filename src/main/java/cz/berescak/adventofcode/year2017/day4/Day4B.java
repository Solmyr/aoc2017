package cz.berescak.adventofcode.year2017.day4;

import cz.berescak.adventofcode.utils.FileUtil;

import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;

public class Day4B {
    public static void main(String[] args) {
        System.out.println("TEST (abcde fghij): " + resolve("abcde fghij"));
        System.out.println("TEST (abcde xyz ecdab): " + resolve("abcde xyz ecdab"));
        System.out.println("TEST (a ab abc abd abf abj): " + resolve("a ab abc abd abf abj"));
        System.out.println("TEST (iiii oiii ooii oooi oooo): " + resolve("iiii oiii ooii oooi oooo"));
        System.out.println("TEST (oiii ioii iioi iiio): " + resolve("oiii ioii iioi iiio"));

        int result =  FileUtil.getResourceLineStream("day4")
                .mapToInt(line -> resolve(line))
                .sum();

        System.out.println("RESULT: " + result);

    }

    private static int resolve(String input) {
        Set<String> words = new TreeSet<>();

        for (String word : input.split("\\s+")) {
            String sortedWord = sort(word);
            if(words.contains(sortedWord)) {
                return 0;
            }

            words.add(sortedWord);
        }

        return 1;
    }

    private static String sort(String word) {
        char[] chars = word.toCharArray();
        Arrays.sort(chars);
        return new String(chars);
    }
}
