package cz.berescak.adventofcode.year2017.day11;

public class HexGrid {
    private int[][] matrix;
    private int halfSize;
    private int x;
    private int y;


    public HexGrid(int size) {
        this.halfSize = size / 2;
        matrix = new int[2 * halfSize][2 * halfSize];
        x = halfSize;
        y = halfSize;
    }

    public void goN() {
        y--;
    }

    public void goS() {
        y++;
    }

    public void goNW() {
        if (x % 2 == 0) y--;
        x--;
    }

    public void goSW() {
        if (x % 2 == 1) y++;
        x--;
    }

    public void goSE() {
        if (x % 2 == 1) y++;
        x++;
    }

    public void goNE() {
        if (x % 2 == 0) y--;
        x++;
    }


    public int stepFromStart() {
        int steps = Math.abs(x - halfSize);
        int tempy = Math.abs(y - halfSize) - Math.abs(x - halfSize);
            if (Math.abs(x - halfSize) % 2 == 1) tempy++;
        if (tempy < 0) tempy = 0;
        steps += tempy;

        return steps;
    }

}
