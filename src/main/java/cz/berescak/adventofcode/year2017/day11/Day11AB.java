package cz.berescak.adventofcode.year2017.day11;

import cz.berescak.adventofcode.utils.FileUtil;

public class Day11AB {
    public static void main(String[] args) {
        FileUtil.getResourceLineStream("day11test")
                .forEach(line -> resolve(line));

        FileUtil.getResourceLineStream("day11")
                .forEach(line -> resolve(line));
    }

    private static void resolve(String line) {
        String[] array = line.split(",");
        HexGrid grid = new HexGrid(1000);
        System.out.println(line);
        int max = 0;

        for (int i = 0; i < array.length; i++) {
            switch (array[i]) {
                case "n": grid.goN();break;
                case "s": grid.goS();break;
                case "nw": grid.goNW();break;
                case "sw": grid.goSW();break;
                case "ne": grid.goNE();break;
                case "se": grid.goSE(); break;
                default:
                    System.out.println("CHYBA");
            }

            int actualSteps = grid.stepFromStart();
            if(actualSteps > max) max = actualSteps;

        }

        System.out.println("steps: " + grid.stepFromStart() + " max: " + max);

    }
}
