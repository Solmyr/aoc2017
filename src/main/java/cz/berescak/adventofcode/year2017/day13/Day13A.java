package cz.berescak.adventofcode.year2017.day13;

import cz.berescak.adventofcode.utils.FileUtil;

import java.util.Arrays;
import java.util.stream.Stream;

public class Day13A {
    public static void main(String[] args) {
        resolve(FileUtil.getResourceLineStream("day13test"), 7);
        resolve(FileUtil.getResourceLineStream("day13"), 97);
    }

    private static void resolve(Stream<String> lines, int len) {
        int[] layersMax = new int[len];
        int[] layersActual = new int[len];


        lines.forEach(line -> {
            String[] array = line.split(":");
            layersMax[Integer.parseInt(array[0])] = Integer.parseInt(array[1].replace(" ", "")) - 1;
        });

        int sum = 0;
        for (int i = 0; i < len;) {
            if(layersMax[i] > 0 && layersActual[i] == 0 ) {
                //System.out.println("Scan na vrstve: " + i);
                sum += (layersMax[i] + 1) * i;
            }
            i++;
            for (int j = 0; j < len; j++) {
                int layerLen = layersMax[j];
                if(layerLen > 0) {
                    if(i / layerLen % 2 == 0) {
                        layersActual[j] = i % layerLen ;
                    } else {
                        layersActual[j] = layerLen - (i % layerLen);
                    }
                }

            }
            System.out.println(Arrays.toString(layersActual));

        }
        System.out.println("SUM: " + sum);

    }
}
