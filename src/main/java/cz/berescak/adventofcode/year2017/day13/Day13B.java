package cz.berescak.adventofcode.year2017.day13;

import cz.berescak.adventofcode.utils.FileUtil;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.stream.Stream;

public class Day13B {
    public static void main(String[] args) {
        resolve(FileUtil.getResourceLineStream("day13test"), 7);
        resolve(FileUtil.getResourceLineStream("day13"), 97);
    }

    private static void resolve(Stream<String> lines, int len) {
        int[] layersMax = new int[len];

        lines.forEach(line -> {
            String[] array = line.split(":");
            layersMax[Integer.parseInt(array[0])] = Integer.parseInt(array[1].replace(" ", "")) - 1;
        });
        for (int i = 0; i < 10000000; i++) {
            boolean status = subResolve(i, layersMax, len);
            if(status) {
                System.out.println("OK");
                break;
            }
        }

    }

    private static boolean subResolve(int delay, int[] layersMax, int len) {
        System.out.println("delay: " + delay);
        int[] layersActual = new int[len];

        for (int i = 0; i < len;) {
            for (int j = 0; j < len; j++) {
                int layerLen = layersMax[j];
                if(layerLen > 0) {
                    if((i+delay) / layerLen % 2 == 0) {
                        layersActual[j] = (i+delay) % layerLen ;
                    } else {
                        layersActual[j] = layerLen - ((i+delay) % layerLen);
                    }
                }
            }

            if(layersMax[i] > 0 && layersActual[i] == 0 ) {
                return false;
            }
            i++;


        }
        return true;
    }



}
